var firebaseConfig = {
    apiKey: "AIzaSyACdfgVWPDVpgXsr08TKykBhe15hg7ETIM",
    authDomain: "fir-dashboard-c1cf5.firebaseapp.com",
    databaseURL: "https://fir-dashboard-c1cf5.firebaseio.com",
    projectId: "fir-dashboard-c1cf5",
    storageBucket: "fir-dashboard-c1cf5.appspot.com",
    messagingSenderId: "322827837034",
    appId:"1:322827837034:web:9e428996b5a44698dcf413"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  firebase.auth.Auth.Persistence.LOCAL;

  

  $("#btn-login").click(function()
  {
      var email =  $("#email").val();
      var password =  $("#password").val();

      if(email !="" && password != "")
      {
         var result = firebase.auth().signInWithEmailAndPassword(email,password);
         result.catch(function(error)
         {
             var errorCode =error.code;
             var errorMessage=error.message;
             console.log(errorCode);
             console.log(errorMessage);
             window.alert("Message : " + errorMessage);

             });
      }
      else{
         window.alert("Please Enter the email and Password");
      }

  });

  $("#btn-signup").click(function()
  {
      var email =  $("#email").val();
      var password =  $("#password").val();
      var cPassword = $("#confirmPassword").val();

      if(email !="" && password != "" && cPassword != "")
      {
         if(password == cPassword){
            var result = firebase.auth().createUserWithEmailAndPassword(email,password);
            result.catch(function(error)
            {
                var errorCode = error.code;
                var errorMessage=error.message;
                window.alert("Message : " + errorMessage);
                });
         }
         else{
             window.alert("Password donot Match with the Confirm Password");
         }
      }
      else{
         window.alert("Please Enter the email and Password");
      }

  });



  $("#btn-resetPassword").click(function()
  {
    var auth = firebase.auth(); 
    var email =  $("#email").val();
    if(email != ""){
           auth.sendPasswordResetEmail(email).then(function(){
                  window.alert("Email has been sent to you , Please check and verify.");
           })
           .catch(function(error){
                    var errorCode =error.code;
                    var errorMessage=error.message;
                    window.alert("Message : " + errorMessage);
                    });
    }else{
        window.alert("Please Enter the email first");
    }
  });


  $("#btn-logout").click(function()
  {
     firebase.auth().signOut(); 

  });

  
